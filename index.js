
let trainer = {
	name: 'Cindy',
	age: 25,
	pokemon: ['Pikachu','Charizard','Balbasaur', 'Groudon'],
	friends: { hoenn : ['May','Max'],
	kanto: ['Brock' , 'Misty']
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you");
	},
}

console.log(trainer)
console.log('Result of dot notation:')
console.log(trainer.name)
console.log('Result of square bracket notation:')
console.log(trainer['pokemon'])
console.log('Result of talk method')
trainer.talk()

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods 
	this.tackle = function(target){
		console.log(target.name + " tackled " + this.name);
		let newHealth = (this.health - target.attack)
		console.log(this.name + "'s health is reduced to " + (this.health - target.attack))
	
	 	if( newHealth <= 0){
	 		this.faint()
	 	}
	}
	this.faint = function(){
		console.log(this.name + ' fainted')
	}
}

let pikachu = new Pokemon("Pikachu", 3)
let swampert = new Pokemon("Swampert", 10)
let blaziken = new Pokemon("Blaziken", 50)
let rayquaza = new Pokemon("Rayquaza", 100)
console.log(pikachu)
console.log(swampert)
console.log(blaziken)
console.log(rayquaza)
rayquaza.tackle(blaziken)
pikachu.tackle(swampert)
